<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $category
 * @property string $author
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['category', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'author'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
	
		public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
		
		//new record save to all else save only to updated fields
        if ($this->isNewRecord)
		{
			$this->created_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->created_by = Yii::$app->user->identity->id;
			$this->updated_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->updated_by = Yii::$app->user->identity->id;
		}else{
			$this->updated_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->updated_by = Yii::$app->user->identity->id;
		}
		
		

        return $return;
    }	
	   
//Defenition of relation to user table
 	

	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
	public function getUserAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
/*	public function getCategotyItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
   } */
	


 
	
		 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
}
